#!/bin/bash

dir="C:\Users\wlim\Desktop\gitlab_DA\split_file";
file="crash_catalonia.csv";
split_num=4;
header=true;

#assign the base name for resultant split file
basename=`echo "$file" | cut -f 1 -d '.'`

cd $dir

mkdir resultant_files/

if [ $header == true ]
then 
	echo "Header: Yes"
	echo "command: split -nl/$split_num $file"
	echo "Splitting file"
	
	# split files
	split -nl/$split_num $file ./resultant_files/
	
	# create header file
	echo "Getting header row"
	head -n 1 $file > header 
	
	# cd into resultant_files
	cd resultant_files
	
	#initalise counter
	counter=1
	
	# loop through each file and add header
	for files in "$dir/resultant_files/"*; do
	
		if [ $files == $dir/resultant_files/aa ]
		then 
			# just rename aa file
			mv aa ${basename}_Part_${counter}.csv
			echo "Renaming file number $counter of $split_num"
	
		elif [ $files != $dir/resultant_files/aa ]
		then
			# counter + 1
			((counter++))
			
			# cat header on it
			cat ../header $files > ${basename}_Part_${counter}.csv
			
			# remove original files
			rm $files
			
			echo "Renaming file number $counter of $split_num"
		fi
	done
	
elif [ $header == false ]
then 
	echo "Header: None"
	echo "command: split -nl/$split_num $file"
	echo "Splitting file"
	
	# split files
	split -nl/$split_num $file ./resultant_files/
	
	# cd into resultant_files
	cd resultant_files
	
	#initalise counter
	counter=0
	
	# loop through each file 
	for files in "$dir/resultant_files/"*; do
	
		# counter + 1
		((counter++))
		
		#rename each file
		mv $files ${basename}_Part_${counter}.csv
		
		echo "Renaming file number $counter of $split_num"
	
	done
fi 

echo "Splitting completed"