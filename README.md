# File Splitting tool

Simple bash script to automate file splitting to smaller files

# How to use
There are 4 vairables that needs to be changed within the bash script
1. Open `split_files.sh` with Notepad++ or any editor.
2. Rename the `directory` where the file is located.
3. Rename the `filename`.
4. Set the `number` of resultant files you want (number of splits).
5. Specify if there is a header in the file. `true` or `false`

P.S. you can you this command to see the 1st row: `head -n 1 <filename.txt>`
